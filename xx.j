const express = require("express");
const fs = require("fs");
const superagent = require("superagent");
const config = require("./xcx_config");
const app = express();
const url = `https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${config.appid}&secret=${config.secret}`;


app.get("/",(req,res) => {
    res.json({code:1,msg:"success"})
    superagent.get(url, (err,result) => {
        if (err) {
            console.log(err)
        }else{
            let baseUrl = `https://api.weixin.qq.com/wxa/getwxacode?access_token=${JSON.parse(result.text).access_token}`;
            // console.log(baseUrl)
            let json = {
                "scene": "id=1",
                "path": "pages/index/index",
                "width": 430,
                "auto_color": false,
                "line_color": {
                    "r": "0",
                    "g": "0",
                    "b": "0"
                },
                "is_hyaline": false
            };

            superagent.post(baseUrl).send(JSON.stringify(json)).end( (err,result) => {
                if (err){
                    console.log(err)
                } else {
                    // console.log(result.body);
                    fs.writeFile("./images/code.png",result.body,(err) => {
                        if (err) {
                            console.log(err)
                        }else{
                            console.log("保存成功")
                        }
                    })

                }
            })
        }
    });
})
app.get("/getImage",(req,res) => {
    res.sendFile(__dirname + "/images/code.png")
})
app.listen(4000,() => {
    console.log(`开启....`)
})