var mongoose=require( '../common/db'); //数据库连接文件
var user=new mongoose.Schema({             //建立数据表 即是集合
   username:String,
   password:String,
   userMail:String,
   userPhone:String,
   userAdmin:Boolean,
   userPower:Number,
   userStop:Boolean
})

  user.statics.findAll=function(callback){    //写方法 分模块的方法是在MODEL里
   this.find({} ,callback);
  };
  
  user.statics.findByUsername=function(name,callBack){ //参数 函数
   this.find({ username:name},callBack);
  };
   

  user.statics.findUserLogin=function(name,password,callBack){
     this.find({username:name, password:password,userStop:false}, callBack);//用户调用这些方法 传参数进来，并且执行回调
     };
 

     user.statics.findUserPassword=function(name,mail,phone,callBack){
        this.find({username:name, userMail:mail,userPhone:phone}, callBack);
        };



    
   var userModel=mongoose.model('user',user);

   module.exports=userModel;